
const Ajv = require('ajv');

const INVALID_TEMPLATE_ERROR = 'INVALID MESSAGE TEMPLATE';
const SCHEME_QUIZ = 'QUIZ';
const SCHEME_PROVA = 'PROVA';
const SCHEME_NIVELAMENTO = 'NIV';
const SCHEME_ATIVIDADE_COMPLEMENTAR = 'ATVCOM';
const SCHEME_ED_MATRIZ = 'AUTED';
const SCHEME_STUDENT_ENROLL = 'STUDENT_ENROLL';
const SCHEME_SEGCHAM = 'SEGCHAM';
const SCHEME_RECTIFICATION = 'RECTIFICATION';
const SCHEME_PRESENCE = 'PRESENCE';
const SCHEME_EXAM = 'EXAM';
const SCHEME_CHARGE = 'CHARGE';

const SCHEME_NIVELAMENTO_ID = 'schemaNiv';

const schemeMessageCharge = {
  "$id": "schemeMessageCharge",
  "type": "object",
  "properties": {
    "type_event": { "type": "string" },
    "enroll": { "$ref": "schemaEnrollCharge" },
    "student": { "$ref": "schemaOlimpoStudent" },
    "sent_date": { "type": "string" }
  },
  "required": ["type_event", "enroll", "student", "sent_date"]
};

const schemaEnrollCharge = {
		  "$id": "schemaEnrollCharge",
		  "type": "object",
		  "properties": {
		    "unit_id": { "type": "string" },
		    "discipline_code": { "type": "string" },
		    "class_id": { "type": "string" },
		    "semester": { "type": "string" },
		    "dimtip": { "type": "string" }
		  },
		  "required": ["unit_id", "discipline_code", "class_id", "semester","dimtip"]
		};

const schemeOlimpoMessage = {
  "$id": "schemeOlimpoMessage",
  "type": "object",
  "properties": {
    "type_event": { "type": "string" },
    "enroll": { "$ref": "schemaEnroll" },
    "student": { "$ref": "schemaOlimpoStudent" },
    "content": { "$ref": "schemaOlimpoED" },
    "sent_date": { "type": "string" }
  },
  "required": ["type_event", "enroll", "student", "content", "sent_date"]
};

const schemeOlimpoStudent = {
  "$id": "schemaOlimpoStudent",
  "type": "object",
  "properties": {
    "alu_cod": { "type": "string" },
    "esp_cod": { "type": "string" },
  },
  "required": ["alu_cod", "esp_cod"]
};

const schemeOlimpoED = {
  "$id": "schemaOlimpoED",
  "type": "object",
  "properties": {
    "ed_no_periodo": { "type": "integer" }
  },
  "required": ["ed_no_periodo"]
};


const schemaPresence = {
  "$id": "schemaPresence",
  "type": "object",
  "properties": {
    "percfaltas": { "type": "number" },
    "totalfaltas": { "type": "number" },
    "totalaulas": { "type": "number" }
  },
  "required": ["percfaltas", "totalfaltas", "totalaulas"]
};

const schemaAlunoPresence = {
  "$id": "schemaAlunoPresence",
  "type": "object",
  "properties": {
    "alu_cod": { "type": "string" },
    "esp_cod": { "type": "string" },
  },
  "required": ["alu_cod", "esp_cod"]
};

const studentSchema = {
  "$id": "messageImportStudent",
  "type": "object",
  "properties": {
    "student": { "$ref": "schemaAluno" },
    "enroll": { "$ref": "schemaEnroll" }
  },
  "required": ["student", "enroll"]
}

const schemeMessagePresence = {
  "$id": "schemeMessagePresence",
  "type": "object",
  "properties": {
    "type_event": { "type": "string" },
    "enroll": { "$ref": "schemaEnroll" },
    "student": { "$ref": "schemaAlunoPresence" },
    "content": { "$ref": "schemaPresence" },
    "sent_date": { "type": "string" }
  },
  "required": ["type_event", "enroll", "student", "content", "sent_date"]
};

const getSchema = (type) => {
  const requiredProperties = ["type_event", "student", "content", "dynamoId", "sent_date"];

  if (type !== SCHEME_NIVELAMENTO_ID) {
    requiredProperties.push('enroll');
  }

  return {
    "$id": "messageBody",
    "type": "object",
    "properties": {
      "type_event": { "type": "string" },
      "student": { "$ref": "schemaAluno" },
      "enroll": { "$ref": "schemaEnroll" },
      "responsible": { "$ref": "schemaResponsible" },
      "content": { "$ref": type },
      "url_origin": { "type": "string" },
      "dynamoId": { "type": "string" },
      "sent_date": { "type": "string" }
    },
    "required": requiredProperties
  }
};

const schemaEnroll = {
  "$id": "schemaEnroll",
  "type": "object",
  "properties": {
    "unit_id": { "type": "string" },
    "discipline_code": { "type": "string" },
    "class_id": { "type": "string" },
    "semester": { "type": "string" }
  },
  "required": ["unit_id", "discipline_code", "class_id", "semester"]
};



const schemaAluno = {
  "$id": "schemaAluno",
  "type": "object",
  "properties": {
    "cpf": { "type": "string" },
    "alu_cod": { "type": "string" },
    "username": { "type": "string" },
    "esp_cod": { "type": "string" },
    "name": { "type": "string" }
  },
  "required": ["cpf", "alu_cod", "esp_cod", "username", "name"]
};

const schemaResponsible = {
  "$id": "schemaResponsible",
  "type": "object",
  "properties": {
    "cpf": { "type": "string" },
    "username": { "type": "string" },
    "name": { "type": "string" }
  },
  "required": ["cpf", "username", "name"]
};

const schemaQuiz = {
  "$id": "schemaQuiz",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "activity_name": { "type": "string" },
    "qtt_questions": { "type": "integer", "minimum": 1 },
    "qtt_questions_right": { "type": "integer" },
    "qtt_questions_done": { "type": "integer" }
  },
  "required": ["_id", "activity_name", "qtt_questions", "qtt_questions_right", "qtt_questions_done"]
};

const schemaExam = {
  "$id": "schemaExam",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "absent": { "type": "boolean" },
    "value": { "type": ["integer", "null"] }
  },
  "required": ["_id", "absent"]
};


const schemaProva = {
  "$id": "schemaProva",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "absent": { "type": "boolean" },
    "value": { "type": ["integer", "null"] }
  },
  "required": ["_id", "absent"]
};

const schemaNiv = {
  "$id": "schemaNiv",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "name": { "type": "string" },
    "semester": { "type": "string" }
  },
  "required": ["_id", "name"]
};

const schemaAtvCom = {
  "$id": "schemaAtvCom",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "absent": { "type": "boolean" },
    "value": { "type": ["integer", "null"] }
  },
  "required": ["_id", "absent"]
};

const schemaAutEd = {
  "$id": "schemaAutEd",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "shortname": { "type": "string" },
    "complete": { "type": "boolean" }
  },
  "required": ["_id", "complete"]
};

const schemaSegCham = {
  "$id": "schemaSegCham",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "absent": { "type": "boolean" },
    "value": { "type": ["integer", "null"] }
  },
  "required": ["_id", "absent"]
};

const schemaRectification = {
  "$id": "schemaRectification",
  "type": "object",
  "properties": {
    "_id": { "type": "string" },
    "absent": { "type": "boolean" },
    "value": { "type": ["integer", "null"] },
    "scoreUpdateType": { "type": "string" },
    "requestJustificative": { "type": "string" }
  },
  "required": ["_id", "absent", "scoreUpdateType", "requestJustificative"]
};


const buildSchemeError = (scheme, errors) => {
  const message = `The message received not followed the template ${scheme}\n${JSON.stringify(errors)}`
  const error = new Error(message);
  error.type = INVALID_TEMPLATE_ERROR;
  return error;
}

const validate = (worker, jsonData) => {
  let ajv = new Ajv;
  let schema = {};
  let isValid;
  switch (worker) {

    case SCHEME_CHARGE:
      schema = schemeMessageCharge;
      isValid = ajv.addSchema(schemaEnrollCharge)
        .addSchema(schemeOlimpoStudent)
        .compile(schema);
      break;

    case "QUIZ":
      schema = getSchema("schemaQuiz");
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .addSchema(schemaResponsible)
        .addSchema(schemaQuiz)
        .compile(schema);
      break;

    case "PROVA":
      schema = getSchema("schemaProva");
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .addSchema(schemaResponsible)
        .addSchema(schemaProva)
        .compile(schema);
      break;

    case "NIV":
      schema = getSchema(SCHEME_NIVELAMENTO_ID);
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .addSchema(schemaResponsible)
        .addSchema(schemaNiv)
        .compile(schema);
      break;

    case "ATVCOM":
      schema = getSchema("schemaAtvCom");
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .addSchema(schemaResponsible)
        .addSchema(schemaAtvCom)
        .compile(schema);
      break;

    case SCHEME_ED_MATRIZ:
      schema = schemeOlimpoMessage;
      isValid = ajv.addSchema(schemeOlimpoStudent)
        .addSchema(schemeOlimpoED)
        .addSchema(schemaEnroll)
        .compile(schema);
      break;

    case "STUDENT_ENROLL":
      schema = studentSchema;
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .compile(schema);
      break;

    case "SEGCHAM":
      schema = getSchema("schemaSegCham");
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .addSchema(schemaResponsible)
        .addSchema(schemaSegCham)
        .compile(schema);
      break;

    case SCHEME_EXAM:
      schema = getSchema("schemaExam");
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .addSchema(schemaResponsible)
        .addSchema(schemaExam)
        .compile(schema);
      break;

    case SCHEME_PRESENCE:
      schema = schemeMessagePresence;
      isValid = ajv.addSchema(schemaEnroll)
        .addSchema(schemaAlunoPresence)
        .addSchema(schemaPresence)
        .compile(schema);
      break;

    case SCHEME_RECTIFICATION:
      schema = getSchema("schemaRectification");
      isValid = ajv.addSchema(schemaAluno)
        .addSchema(schemaEnroll)
        .addSchema(schemaResponsible)
        .addSchema(schemaRectification)
        .compile(schema);
      break;

  }

  const valid = isValid(jsonData);
  return { success: valid, error: valid ? undefined : buildSchemeError(worker, isValid.errors) }
};

const validateP = (worker, jsonData) => {
  return new Promise((resolve, reject) => {
    const result = validate(worker, jsonData);
    if (result.success) {
      return resolve(result.success);
    }

    return reject(result.error);
  });
};


module.exports.SCHEME_QUIZ = SCHEME_QUIZ;
module.exports.SCHEME_PROVA = SCHEME_PROVA;
module.exports.SCHEME_NIVELAMENTO = SCHEME_NIVELAMENTO;
module.exports.SCHEME_ATIVIDADE_COMPLEMENTAR = SCHEME_ATIVIDADE_COMPLEMENTAR;
module.exports.SCHEME_ED_MATRIZ = SCHEME_ED_MATRIZ;
module.exports.SCHEME_STUDENT_ENROLL = SCHEME_STUDENT_ENROLL;
module.exports.SCHEME_SEGCHAM = SCHEME_SEGCHAM;
module.exports.SCHEME_PRESENCE = SCHEME_PRESENCE;
module.exports.SCHEME_RECTIFICATION = SCHEME_RECTIFICATION;
module.exports.SCHEME_EXAM = SCHEME_EXAM;
module.exports.SCHEME_CHARGE = SCHEME_CHARGE;
module.exports.SCHEME_ERROR = INVALID_TEMPLATE_ERROR;
module.exports.buildSchemeError = buildSchemeError;
module.exports.validateP = validateP;
module.exports.validate = validate;